# Evolutionary Systems Applied to Robotics

- Professor: Eduardo do Valle Simões
- Email: simoes@@@@@@icmc.usp.br  (Obs.: just one @)
- Department of Computer Sistemas– ICMC - USP
- Group of Embedded and Evolutionary Systems
- Laboratory of Reconfigurable Systems

## Portuguese Version: [here](https://gitlab.com/simoesusp/disciplinas/-/blob/master/SSC0713-Sistemas-Evolutivos-Aplicados-a-Robotica/README.md)

## 2024 Students – Second Semester
- Attendance list – Please sign the list every class 
- Every code and material developed in classes will be available in this platform. 

## Assessment of student performance
- The assessment of student performance will be consisted of the presentation of a project consisting of the implementation of an evolutionary algorithm applied to any real problem.
- The students will be assigned to groups of 3-5 members for the development of the project.
- The project will be developed in group, but the individual grades will come from the evaluation of student performance in the project.
- EVERY STUDENT WILL HAVE TO BE PRESENT IN THEIR PROJECT PRESENTATION!!

### Project presentation for 2024 – Second semester:
-	Project presentation will be done in a meeting with the professor in the classroom  in the last 3 weeks of the semester: choose a proper day and time in the following table: ==> LINK DOCS: https://docs.google.com/spreadsheets/d/1_ApKDWWo3zTJVUvQOwhrquvahLiqaPpVBGVFXWG8tdY/edit?usp=sharing

- Insert in the table your project title, the names of the students, your USP Id, and a link for the project at github/gitlab 

- The project listed on github/gitlab must contain the software and a Readme file containing the application description and information on how to install all the libraries. 
-  the Project on github/gitlab also must have a link (youtube or GoogleDrive) to a video where the students present their project and describe the evolutionary algorithm. 

## Breno´s OpenGL tutorial - https://github.com/Brenocq/OpenGL-Tutorial

## Other Student code and applications:
- how to use Gnuplot inside your own program - https://github.com/NathanTBP/SSC0713_Sistemas-Evolutivos-Aplicados-a-Robotica_Nathan/tree/master/Tutoriais

- EA to evaluate mathematical functions - https://github.com/NathanTBP/SSC0713_Sistemas-Evolutivos-Aplicados-a-Robotica_Nathan/tree/master/Tutoriais

- Code of a Multi-layer Perceptron Neural Network - MLP - https://github.com/ncalsolari/Rede-Neural-Generica-3-camadas-

- Lucas´ EAs (Experimentos iniciais com variacao da mutacao e da funcao) - https://github.com/LucasRorisCube/SistemasEvolutivos

# Recorded classes (2021 – second semester)

- Class01 20/08/2021 (14:20h) - https://drive.google.com/file/d/1z0wHTwafRw7nwQTY3KkOe3I3OxQAmEqq/view?usp=sharing

- Class02 27/08/2021 (14:20h) - https://drive.google.com/file/d/1Fs3FRpP9Lc7Xapo_hsEGUy2lUowoZhnA/view?usp=sharing
  - Link pro git dos AGs do Lucas - https://github.com/LucasRorisCube/SistemasEvolutivos

- Class03 03/09/2021 (14:20h) - https://drive.google.com/file/d/1uQqhGOfT46z6nad3q2lEvz49gr2bH9e1/view?usp=sharing

- Class04 10/09/2021 (14:20h) - https://drive.google.com/file/d/1tpfDFrb5jM8fA0mTg3eJcncDAH0btiUf/view?usp=sharing

- Class05 17/09/2021 (14:20h) - https://drive.google.com/file/d/1-ooVRlmOONFZhiUNCR6dh9enuDSUEzkY/view?usp=sharing

- Class06 24/09/2021 (14:20h) - https://drive.google.com/file/d/1Xf0J30Bj8yAWYjvh7oZ-nwebZ2Qwrv2G/view?usp=sharing

- Class07 08/10/2021 (14:20h) - https://drive.google.com/file/d/1N5rFnOVK_dhh3yGfL7NcbMQJMH93C-fJ/view?usp=sharing

- Class08 15/10/2021 (14:20h) - https://drive.google.com/file/d/1UQzIMW8NrJ-ZT8Y07D48_-ig5ITOXcdh/view?usp=sharing

- Class09 22/10/2021 (14:20h) - https://drive.google.com/file/d/1hk7EG5txVCH6fR2i_P7lpe3vyaZT91D-/view?usp=sharing

- Class10 29/10/2021 (14:20h) - https://drive.google.com/file/d/1GlBfaybrS_GHhazd3vBzfk2ZEqGyMNVP/view?usp=sharing

- Class11 12/11/2021 (14:20h) - https://drive.google.com/file/d/1LJnjQ9d0hDZimffYxOUyI-Mlp9NRyk3d/view?usp=sharing

- Class12 19/11/2021 (14:20h) - https://drive.google.com/file/d/1faWAVpnvNxBpCBb0ixFqgkFa0qt31k4Y/view?usp=sharing

- Class13 26/11/2021 (14:20h) - https://drive.google.com/file/d/1w8muGkS9XSUk5HtgEW8dZaGQhkn4OuFU/view?usp=sharing
  
  - NVIDIA’s physics simulation environment for reinforcement learning research - https://developer.nvidia.com/isaac-gym

  - Atta - a robot simulator for 2D and 3D applications (Breno) - https://github.com/brenocq/atta

- Class14 03/12/2021 (14:20h) - https://drive.google.com/file/d/1IydWtxJlGuDXQ3URtGgXkPKWrgr-xe1e/view?usp=sharing

- Class15 10/12/2021 (14:20h) - https://drive.google.com/file/d/1OqYVZPIoZwhr07bjl82ThvHZOwtCTI7W/view?usp=sharing

- Class16 17/12/2021 (14:20h) - https://drive.google.com/file/d/1uB0poci_FSbe4nfAXMvGrurw-VYQMLp7/view?usp=sharing
  - EA solving the Magic cube - https://drive.google.com/file/d/1m1ASuEiF58ZIPgMz9ynGbPV5PQQAcZUd/view?usp=sharing

- Class17 07/01/2022 (14:20h) - https://meet.google.com/btu-crez-aro



## List of Projets from 2023 - Seccond Semester:

- Simulação de um Ecossistema	https://github.com/EnzoTM/Ecossistema/
- Jogo da Velha²	https://github.com/joseCarlosAndrade/NeuralNetwork-AG
- Rainhas	https://github.com/vrr03/Rainhas
- Darwinism Survival Shooter	https://github.com/RafaelLearth/Algoritmos_Evolutivos
- Campo Minado	https://github.com/trizcard/evolutionary-algorithm-for-minesweeper.git
- Otimizações	MuriloLirani/SCC0713---SisEvolutivos (github.com)
- Árvores de Decisão Genéticas	https://github.com/Rafaelsoz/Projeto_SSC_0713_Sistema_Evolutivos
- Camuflagem	https://github.com/pdrtorrente/Sistemas-Evolutivos
- Evolutive RPG Chars	https://github.com/gasottorodrigues/evolutive-rpg-chars
- Fototropismo Evolutivo	https://github.com/LVinaud/fototropismo
- Fórmula 1	https://github.com/yp1plus/F1-EvolutiveSystem
- Caixeiro 4 Dummies	https://github.com/rhaynacasado/caixeiro4dummies
- Minimização de Erro em Ondas Parciais	https://github.com/fda-tome/ea-bscsim/tree/main
- MOSAICO (Simulador de Sobrevivência)	https://github.com/calixtojp/algsEvolutivos
- Unity 2D Evolutionary Balancing Project	https://github.com/RodrigoLimaRFL/AI-learns-to-stand-up-2d-Evolutionary-Algorithm
- Panóptico Evolutivo	https://github.com/lucaslimaromero/Panoptico-Evolutivo
- Imagens Evolutivas	https://github.com/marcogarcia2/sistemas-evolutivos
- Optimal Racing Line	https://github.com/AndreyCortez/RacelineOptimization
- Controle Drone	https://github.com/TeOSobrino/EvolSis
- Raiz de Funções	https://github.com/LucasStapf/rootfn
- Squares and Beans	https://github.com/m-almeida0/SquaresAndBeans



## List of Projets from 2021 - Seccond Semester:

- 1	Enigma Decrypt with EA -	https://github.com/Guerreiro51/Enigma-Decrypt-with-EA
- 2	Projeto Monalisa -	https://github.com/TsuyoshiSonobe/SSC0713-SistemasEvolutivos
- 3	Micromouse Evolutivo -	https://github.com/faffonso/micromouse-evolutivo
- 4	Vacina Viajante	LeonardoPradoDias/Vacina-Viajante - https://github.com/LeonardoPradoDias/Vacina-Viajante
- 5	FunctionMaximumCalculator -	https://github.com/cesar-guibo/FunctionMaximumCalculator
- 6	SmartDotsGA -	https://github.com/sprmbng/smartDots
- 7	Evolutive PID Tuning -	https://github.com/andrecoco/PIDTuningEvolutivo
- 8	Problema do caixeiro viajante -	https://github.com/ThiagoRGoveia/ag-traveling-merchant
- 9	Subset Sum Problem -	https://github.com/costaluis/SistemasEvolutivos
- 10	NRainhas -	https://github.com/andrebpradof/Sistemas-Evolutivos
- 11	Rubiks Cube Solver -	https://github.com/LucasRorisCube/RubiksCubeSolver
- 12	EvolvePy -	https://github.com/EltonCN/evolvepy
- 13	Evolutionary Travelling Salesman -	https://github.com/Float07/evolutionary-traveling-salesman/tree/usp
- 14	GeneticImages -	https://github.com/Franreno/GeneticImages
- 15	Evolutionary fish -	https://github.com/kenzonobre/Evolutionary-fish
- 16	Natural Selection Simulator -	https://github.com/GeorgeGantus/NaturalSelectionSimulator



## List of Projets from 2021 - First Semester:

- Grupo 1- Teris Evolutivo - Tiago Triques -  https://gitlab.com/simoesusp/teris-evolutivo

- Grupo 2 - Flechas ao Alvo - Luan Ícaro Pinto Arcanjo, Rodrigo Cesar Arboleda, Victor Graciano de Souza Correia - https://github.com/victorgscorreia/Evolutivos

- Grupo 3  - Snake evolutivo - Felipe Barbosa - https://github.com/felipeb-oliveira/Evolving-snakes

- Grupo 4 - DINO google evolutivo - Ana Clara Amorim Andrade - https://github.com/AnaClaraAmorim/Dino-evolutivo

- Grupo 5 - Evolução de Robôs Manipuladores - Henrique Caetano e Heitor Masson - https://github.com/heitormasson/RobotFinder

- Grupo 6 - Brian Chuquiruna Leon - Evolução de Robô com Campos Potenciais
https://github.com/BrianChuquirunaLeon/AlgoritmosEvolutivos_CamposPotenciais 

- Grupo 7 -  O Jogo Mais Difícil do Mundo - João Victor Garcia Coelho, Pedro Henrique Conrado, Victor Gomes de Carvalho - https://github.com/JVGC/Worlds-Hardest-Game-GA

- Grupo 8 -  Pong Neural - Jayro Boy e Isadora Siebert - JayroBoy/pongNeural: Trabalho para a disciplina ssc0713. Feito em conjunto com a @isadorasiebert (github.com)

- Grupo 9 - Desvio de obstáculos evolutivo - Gabriel de Andrade Dezan, Ivan Mateus de Lima Azevedo - https://github.com/gdezan/genetic-obstacles

- Grupo 10 - Maze Evolutivo - Gustavo Vinicius Vieira Silva Soares https://github.com/gsoares1928/ProjEvolutivos

- Grupo 11 - Plinko Evolutivo - Felipe Guilermmo Santuche Moleiro - https://github.com/FelipeMoleiro/PlinkoEvolutivo

- Grupo 12 - COmpetição IEEE - Lucas Toschi, Matheus Silva, Murilo - https://github.com/toschilt/IEEEOpenEvolutAlg

- Grupo 13 - Gerador de músicas baseado em exemplos - Filipe Oliveira, Henrique Núñez https://github.com/henriquenunez/21-3.14lots


## List of Projets from 2020 - Seccond Semester:

- Automatic adjustment of game parameters - Lucas Yuji Matubara, Vinícius Eduardo de Araújo, Pedro Guerra Lourenço, Marcus Vinicius Castelo Branco Martins - https://github.com/AraujoVE/OSRobotsGame
- Agar.io - Andre Santana Fernandes, Diogo Castanho Emídio, Leonardo Antonetti da Motta, Marcus Vinicius Santos Rodrigues, Olavo Morais Borges Pereira - https://github.com/AndreSFND/evolutive-agario - (RNA: https://github.com/l-a-motta/neural-network-base)
- Status de RPG Evolutivo - Luca Gomes, Vinicius Finke, Igor Lovatto, Rafael Velloso - https://gitlab.com/RafMV/status-de-rpg-evolutivo
- Poker Evolutivo - Thiago Henrique Vicentini, Rafael Kuhn Takano - https://github.com/ThiagoVicentini/PokerEvolutivo
- Evolutive Level Generation - Diany Pressato, Matheus da Silva Araujo, Michelle Wingter - https://github.com/M-A-rs/Evolutionary-Maze-Generation
- TSP(Travelling Salesman Problem) - Alberto Neves, Mathias Fernandes, Marcus Vinícius Medeiros Pará - https://github.com/Math-O5/Genetic-Travelling-Saleman-Problem
- Asteroids - Guilherme Hideki Tati, Leonardo Kobe, Joao Pedro Reis - https://github.com/guilhermeht/Asteroids_evolutionaryAlgorithms
- Simulating an Ecosystem - Paulo Henrique da Silva, Gustavo Tuani Mastrobuono - https://github.com/gumastro/ecosystem_simulation
- Genetic Shortest Path - Luiz Fernando S. E. Santos - https://github.com/LFRusso/genetic-shortest-path
- Seguidor de Linha Evolutivo - Joao Marco Barros, Matheus Borges Kamla  - https://github.com/MatheusBorgesKamla/Seguidor_de_Linha_Evolutivo 
- Wild Life Simulation - Henrique Tadashi Tarzia, Victor Akihito Kamada Tomita - https://github.com/Vakihito/Wild-Life-Simulation.git
- Jogo da Velha - Eduardo Souza Rocha - https://github.com/Edwolt/Jogo-da-Velha
- Basquete Evolutivo - Nelson Calsolari Neto - https://github.com/ncalsolari/BasqueteEvolutivo
- Shortest Path Genético - Pedro Henrique Nieuwenhoff - https://github.com/npdr/GeneticAlgorithm---Shortest-Path
- Reconhecimento Evolutivo de Strings - Nilo Conrado Messias Alves Cangerana, Gabriel Caurin Correa - https://github.com/nilocangerana/trab-algoritmos-evolutivos-2020
- Sistema Diferencial - Matheus Branco Borella, Natan Bernardi Cerdeira - https://github.com/DarkRyu550/Evo
- Root of a Function - André Luis Storino Junior - https://github.com/andrestorino/AG-Root-of-a-Function
- AGKnapsack - Gabriel Victor, João Marcos, Matheus Cunha - https://github.com/matheushw/AGKnapsack
- Máximo e mínimos de funções reais - Nathan Rodrigues de Oliveir - https://github.com/NathanTBP/SSC0713_Sistemas-Evolutivos-Aplicados-a-Robotica_Nathan/tree/master/Vfinal



## List of Projets from 2020 - First Semester:

- AGente Duplo - Guilherme Amaral Hiromoto, Paulo Matana da Rocha, Carlos Henrique Lima Melara - https://gitlab.com/charles2910/projetos-de-sistemas-evolutivos-ssc0713
- MoonlitEvolution: Matheus Tomieiro, Yago Poletto  - https://github.com/matheustomieiro/MoonlitEvolution
- Evolução com regiões: Gabriel Gama - https://github.com/GSoaresgama/simoes-regioes.git
- TDM-2D: Gabriel Gama - https://github.com/GSoaresgama/TDM-GA_2D
- Labyrinth Escape Evolution - Felipe de Oliveira, Matheus Tomieiro, Victor Vieira Custodio Reis, Yago Poletto - https://github.com/matheustomieiro/lesc-evolution
- N-Axis Robotic Arm Control - Breno Cunha Queiroz - https://github.com/Brenocq/N-AxisRoboticArmControl
- Honeybee Simulation - Breno Cunha Queiroz, Johnny Batista - https://github.com/Brenocq/Honeybee-Simulation
- Neuroevolutionary Investor - Gabriel de Oliveira Guedes Nogueira - https://github.com/Talendar/neuroevolutionary_investor
- Genetic Path Finding Individuals - Alyson Matheus Maruyama Nascimento - https://gitlab.com/alyson1907/genetic-path-finding-individuals
- Canhão Evolutivo - Giovanni Paolo Meloni - https://github.com/Giovanni-P-Meloni/SEAR_JogoGenetico
- Darwin Bot FC - Mateus Prado Santos - https://github.com/matprado/Darwin_Bot_FC
- No-Code GA - Gabriel Santos Souza - https://github.com/gsasouza/no-code-ga
- GAPuzzleGame - Vinicius Henrique Borges, Tiago José de Oliveira  Toledo Junior, Vinícius Nakasone Dilda - https://github.com/TNanukem/GAPuzzleGame
- Filas Evolutivas - Leonardo Alves Paiva, Rafael Pastre - https://github.com/rafael-pastre/SSC0713-Sistemas-Evolutivos 
- Genetic Algorithm for Epidemic Model parameter adjustment - Leonardo Alves Paiva, Rafael Pastre - https://github.com/rafael-pastre/SSC0713-Sistemas-Evolutivos-Epidemico 
- Player’s Best Stats - Rodrigo Mendes, Marcelo Moraes - https://github.com/MarceloMoraesJr/Sistemas-Evolutivos 
- MLP Genetic Tuning - Antonio Moreira, Matheus Alves, Luca Porto - https://github.com/Micanga/mlp_genetic_tuning
- Gerador de inimigos para jogos - Luana Terra, Paolo Scassa - https://github.com/LuTDC/sistemasevolutivos
- Tetris Modificado (work in progress) - Higor Tessari, Gabriel Alfonso Nascimento Salgueiro - https://github.com/Higor-Tz/Tetris
- Jogo da velha evolutivo - Arthur Amêndola Paschoal - https://github.com/Arthur-AP/Algoritmo-genetico-Jogo-da-Velha
- Random String - Alysson Oliveira - https://github.com/alysson-oliveira/Random-String
- Flies FindHome - Paulo Katsuyuki Muraishi Kamimura, Afonso Henrique Piacentini Garcia, Felipe Barbosa de Oliveira
https://github.com/mkatsuyuki/findhome
- Maximum of a Function - Guilherme Eiji Ichibara, Luccas Paroni e Thiago Benine 
https://github.com/thiagobenine/genetic-algorithm
- Smart Rockets - Diego da Silva Parra, Mateus Virginio Silva, Murilo Luz Stucki e Tainá Andrello Piai.
https://github.com/Virgini0/Evolutivos_SmartRockets


## List of Projets from 2019 - Seccond Semester:

- Ecossistema com predador e presa: Ygor Pontelo, Marcelo Temoteo e Matheus Lucas - https://github.com/ygorpontelo/ecosystem
- Teste Feliz - Tiago Triques https://gitlab.com/tiagotriques/banana
- A Saga do Herói - Clara Rosa Silveira, Lucas Fernandes Turci, Raphael Medeiros Vieira https://github.com/lucasturci/ProjetoEvolutivos
- Tom and Jerry - Tiago Marino Silva, Alexandre Galocha Pinto Junior, Eduardo Pirro - https://github.com/firewall1011/tom-and-jerry
- A Lenda de Baiacu - Gyovana Mayara Moriyama, Henrique Matarazo Camillo
https://github.com/HenriqueCamillo/A-Lenda-de-Baiacu/
- PathPlanning with Obstacles - Pedro Natali, Rafael Pinho e Carlos Franco - https://github.com/PedroNatali/Trabalho-Evolutivos
- Flappy Bird - Gustavo Verniano Angélico de Almeida, Caio Ferreira Bernardo - https://github.com/CaioFerreiraB/trab_evolutivos
- Tetris Evolution - Rafael Gongora Bariccatti - https://github.com/bariccattion/Tetris_Evolution
- Problema das N rainhas
https://github.com/felfipe/trabsimoes
- Coevolução: predador e presa - Alice Valença De Lorenci
https://github.com/AliceDeLorenci/pray-predator-coevolution
- Mountain Car - Caio Ostan, Leandro Giusti Mugnaini, Leonardo Vinicius de Oliveira Toledo
https://github.com/LeoToledo/MountainCarGenetico
- Super Gravitron AG Edition - Augusto Ribeiro Castro, Estevam Fernandes Arantes
https://github.com/Es7evam/Super-Gravitron-AG-Edition
- Houdini metaballs - Juliana Crivelli
https://github.com/jumc/houdini-genetic-optimization
- Problema das 8 Rainhas - Guilherme Brunassi Nogima, Leonardo Akel Daher
https://github.com/gbnogima/8queens
- Smart Rockets - Daniel Sivaldi Feres, Gabriel Scalici
https://github.com/GabrielScalici/Foguetes_Sistemas_Evolutivos
- Melhor caminho com obstáculos - Guilherme Blatt, Igor Rodrigues
https://github.com/guilherme-blatt/Caminho-AG
- Flappy Ball - Antonio Sebastian, Lucas Tavares
https://github.com/lucast98/GA---Flappy-Ball
- Evolutionary Line Follower - Helbert Moreira, Marcos Arce, Marianna Karenina, Vinícius Ribeiro
https://github.com/vinicius-r-silva/evolutionary-line-follower
- Dino Google - Óliver Savastano Becker, Gabriela Chavez, Rafael Farias Roque, Gabriel Eluan Calado
https://github.com/Oliver-Becker/ChromeDino_GeneticPlay
- Minigame Tiro ao Alvo - Gabriel Van Loon e Tamiris Tinelli
https://github.com/GabrielVanLoon/ag_minigame
- Pega-Pega Simulator - Rodrigo Bragato Piva
https://github.com/Rodrigo-P/Pega-pega



## List of Projets from 2018 - Seccond Semester:
- Genetic Max Value Calculator: André Fakhoury, Thiago Preischadt, Vitor Santana - https://andrefakhoury.github.io/genetic-max-value-calculator/
- Genetic clustering framework: Daniel Barretto, David Cairuz, João Guilherme Araújo, Luísa Moura - https://github.com/lusmoura/Darwin
- Neuroevolutional Car Racing game: Lucas Mitri, Lucas Marcondes, Luis Ricardo - https://github.com/lucasgdm/neuroevolution-car-racing 🏎️
- NEAT SuperHexagon + gambiarras do Windows: Leonardo Gomes, André Almada, Eduardo Misiuk, Ali Husseinat - https://github.com/leoagomes/super-intelligence
- Simon's Evolution (Tron tira a tampa!): Eleazar Braga, Fabrício Guedes, Gustavo Lopes - https://github.com/fabricio-gf/tira-a-tampa
- Circuit Optimizer: Guilherme Prearo, Gustavo Nicolau, Paulo Augusto, Vianna - https://github.com/gprearo/CircuitOptimizer
- Slither IO - Snake Evolution: Paulo Augusto, Guilherme Prearo, Gustavo Nicolau -https://github.com/Kotzly/Slither_IO_IA
- Elections in Brazil (Apresentacao em Video https://www.youtube.com/watch?v=eg5iIUG49T0) : Marilene Garcia, Maria Luisa do Nascimento - https://github.com/MariaLuisaNascimento/Trabalho-AlgEvo
- Image Aproximator: Bruno Bacelar Abe, Paula Cepollaro Diana - https://github.com/abe2602/EvolvingAlgs
- FlappyBird e Vinho: Andre Daher Benedetti, Marcelo Bertoldi Diani - https://github.com/MarceloBD/sistemasEvolutivos 
- Snake AI: Paulo Pinheiro Lemgruber Jeunon Sousa, Matheus Carvalho Nali - https://github.com/PauloLemgruberJeunon/AI_Snake
- Caixeiro Viajante (TSP): Murilo Baldi, Victor Roberti Camolesi - https://github.com/Murgalha/tsp-ga
- We Are The Robots (ainda incompleto): Atenágoras Souza Silva - https://gitlab.com/atenagoras/We_Are_The_Robots
- Simplex Evolutivo: Bruno Flávo, Edson Yudi, Rafael Amaro Rolfsen - https://github.com/Exilio016/AG-Solver
- Multithreaded Genetic Algorithm: Gabriel Romualdo Silveira Pupo - https://github.com/gabrielrspupo/multithread-ga
- Tic Tac Toe AI: Luís Eduardo Rozante de Freitas Pereira - https://github.com/LuisEduardoR/tic-tac-toe-genetic-algorithm
- Genetic Minimax Checkers/Racing Car Evolution: Gabriel Carvalho, Victor Souza Cezario - https://github.com/victorcezario97/GACheckers.git (Checkers) / https://github.com/GabrielBCarvalho/Evolutionary-Algorithm-Car (Racing Car)
- A.G. com redeus neurais (jogo da velha): Bráulio Bezerra, Samuel Ferreira, João Ramos, Victor Giovannoni - https://github.com/brauliousp/jogo_da_velha
- Vida de Inseto: Danilo Henrique Cordeiro, Gabriel Kanegae Souza, Marcos Vinicius Barros de Lima Andrade Junqueira - https://github.com/Dancorde/evolutionary-steering (link do trabalho rodando: https://dancorde.github.io/evolutionary-steering/)
- Sliding Puzzle AG: Gabriel Toschi de Oliveira, Ana Maia Baptistão, Fernanda Tostes Marana - https://github.com/gabtoschi/slidingpuzzleGA
- Ant Collony: David Souza Rodrigues, Marcos Wendell Souza de Oliveira Santos - https://github.com/MarcosWendell/Algoritmos_Evolutivos
- Tibia Neat: Alex Sander R. Silva - https://github.com/Psycho-Ray/Tibia-Neat
- FlappyBird: Bolinha Preta: Giovanni Attina do Nascimento, Paulo Andre Carneiro - https://github.com/PauloCarneiro99/Flappy-Bird
- Cinturão de Asteroides: Lucas Nobuyuki Takahashi, Marcelo Kiochi Hatanaka, Paulo Renato Campos Barbosa - https://github.com/pauloty/Cinturao-De-Asteroides
- FoguetesEvolutivos: Alexandre Norcia Medeiros, Giovani Decico Lucafo, Luiz Henrique Lourencao - https://github.com/alexandrenmedeiros/FoguetesEvolutivos
- Neurogen Snake: Guilherme Milan Santos - https://github.com/guilhermesantos/neurogen_snake
- lo e 248 AI, Miguel Gardini, João Pedro Mattos, Vitor Rossi Speranza - https://github.com/vrsperanza/2048-AI---Evolutive-algorithm
- Ploty , Vitor Santana Cordeiro, Thiago Preischadt Pinheiro, André Luís Mendes Fakhoury, - https://github.com/andrefak/genetic-algorithm-plotly
- ProblemaMaximizarItensCaminhao, Helen Santos Picoli - https://github.com/helenspicoli/AlgGenProject
- Space Invaders: Guilherme de Pinho Montemovo, Igor Barbosa Grécia Lúcio, Rafael Corradini da Cunha - https://github.com/rafaelcorradini/space-invaders-genetic-ia
- Black Jack, Guilherme dos Reis, Tiago Daneluzzi - https://github.com/daneluzzitiago/21_evolutivo
- Ecossistema com predador e presa: Ygor Pontelo, Marcelo Temoteo e Matheus Lucas - https://github.com/ygorpontelo/ecosystem
